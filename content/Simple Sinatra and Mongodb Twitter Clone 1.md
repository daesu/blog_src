Title: Simple twitter clone with Sinatra and Mongo; Part I 
Date: 2013-01-22 10:20
Category: Tutorials
Tags: Ruby, Mongodb, Development
Slug: simple_sinatra_twitter_clone_1
Summary: Simple twitter clone with Sinatra and Mongo; Part I 

For this article you should have Ruby, Sinatra, Mongo, and Mongo Mapper already installed.

So let's get right down to business.

First create a directory for our new application, I'm going to call mine Flitter.

And the gemfile, adding our requirements and dependencies.

    touch Gemfile
    echo "source :rubygems" >> Gemfile
    echo "gem 'sinatra'" >> Gemfile
    echo "gem 'mongo'" >> Gemfile
    echo "gem 'mongo_mapper'" >> Gemfile
    bundle install

Let's handle the database and the data models. Create a new directory called 'database' in your application folder and create a new file databae.rb. Then, add the following code.

    require 'rubygems'
    require 'date'
    require 'mongo_mapper'
    
    MongoMapper.database = 'flitter'

This imports the libraries that we will use and sets up a new database called 'flitter' with default settings.

Next we will create the User class for our application.

    class User
        include MongoMapper::Document
    
        key :login,         String, :length => (5..10), :unique => true
        key :firstlastname, String, :length => (2..10)
        key :password,      String
        key :email,         String, :length => (5..20),:required => true,:unique => true
    
        timestamps!
    
        attr_accessor :login, :firstlastname, :email # R/W access
        # Make sure email contains an @
        validates_format_of :email, :with => /@/
        validates_format_of :login, :with => /[a-z]/
    
    end

Here we create a new MongoMapper document with the fields we require for our user. ':length' specifies the accepted number of characters accepted to this and if not it will throw an error message which we can catch later on. ':unique' specifies that this field should be unique data and ':required' specifies that this field cannot be empty. (Although ':length' makes this redundant)

'timestamps!' adds created_at and updated_at keys to the document which will be updated automatically when the document is saved. Very useful. 'attr_accessor' creates set and get methods for the fields specified.

And finally 'validates_format_of' specifies that this field must have a certain format. For example, that the email field contains an '@' symbol.

If we create user document with some appropriate values it will look like this;

    #<User _id: BSON::ObjectId('325323564631675'), 
    created_at: 2013-01-27 16:36:05 UTC, email: "johndoe@mail.net", firstlastname: "John Doe", password: "mypassword", login: "johnd", 
    updated_at: 2013-01-27 16:36:05 UTC>

We are storing the password as plain text which isn't really a good idea. So let's change our User class so that the password will be encrypted.

Add "require 'digest/sha1'" to the top of the document to add methods to use the SHA1 hash function and in the User class remove the password key and add the following;

    key :hashPass,         String
    key :salt,             String

'hashPass will hold our hashed password value and salt will hold the salt.

Next we add the following methods to the User class.

    def password=(pass)
            @password = pass
            self.salt = random_string(10) 
            self.hashPass = User.encrypt( @password, self.salt )
        end
    
    def self.encrypt(pass, salt)
            Digest::SHA1.hexdigest(pass + salt) 
        end

These methods will take a user specified password and encrypt it togeather with a random string as the salt.

To generate the random_string add the following code;

    def random_string(len)
        ranStr = ""
        1.upto(len) { ranStr << rand(36).to_s(36) } 
        return ranStr
    end

This simply gets a random character from base36 which contains the numbers 1-9 and letters a-z up to 'len' times and adds that character to the ranStr string.

*note: To improve upon this, you could also get non-alphanumeric characters to add to 'ranStr'.

Now our user will look like;

    User _id: BSON::ObjectId('510557751f15fc1bfc000001'), 
    created_at: 2013-01-27 16:36:05 UTC, email: "johndoe@mail.net", 
    firstlastname: "John Doe", hashPass: "adb47bdef402127bbc306425e7e92dad5c349d50", 
    login: "johnd", salt: "and3xlb7fk", updated_at: 2013-01-27 16:36:05 UTC

Finally we need to define how to authenticate a user when they try to login.

    def self.authenticate(login, pass)
            u = User.first(:login => login)
            return nil if u.nil?
            return u if User.encrypt(pass, u.salt) == u.hashPass
            nil
        end

Here we take two values, login and pass, and compare them against our login and password credentials in our database. If they match then we return the user, if not we return 'nil'.

Now lets create the app to use this new class. Go back to the main directory and create a new file called 'flitter.rb'. Add the code;

    require 'rubygems'
    require 'sinatra'
    require 'mongo_mapper'
    require './database/database'
    
    set :public, File.dirname(__FILE__) + '/'
    enable :sessions

Here we add the required libraries, including the 'database.rb' file we created earlier and enable Sinatra's built in support for simple cookie-based sessions. We also set the ':public' folder to allow Sinatra to find static content.

Now we want the user to be able to sign up for, and login to, our application. Add the following code to 'flitter.rb';

    $currentuser = nil
    
    get '/flitter/?' do 
        @u = session[:user]
        erb :index      
    end

*note: Ending a url with 'foo/?' will get both '/foo' and '/foo/'.

$currentuser is a global variable which will hold the login id of the currently logged in user. When the user directs their browser to '/flitter' we assign the value of session to @u and call 'index.erb'.

Next we will add the code for the signup form;

    get '/flitter/user/signup/?' do
        erb :signup
    end
    
    post '/flitter/user/signup/?' do
        u = User.new
        u.login = params["login"]
        u.firstlastname = params["firstlastname"]
        u.password = params["password"]
        u.email = params["email"]
    
        if u.save
            redirect "/flitter/user/login"
        else
            @error = u.errors.map{|k,v| "#{k}: #{v}"}.join("<br/> ")
            halt erb :signup
        end
    end

This code creates a new User object, u, and assigns the user provided values to it. If it saves successfully we direct the user to a login page. If it does not, we send an error message to be displayed and halt the signup form.

The error checking here is done by MongoMapper. Remember in the 'database.rb' file when we specified ':length => (5..10)' and ':unique => true'. If those checks, or the other checks, fail then 'u.errors' will provide feedback which we can send to the user.

Create a new folder in the root directory of our application called 'views' and make a new file, 'signup.erb';

    <div class="container">
    <form class ="form-signin" method="post" action="/flitter/user/signup">
    <h3>Signup here!</h3><hr>
    <label class="control-label" for="username">Username</label><br>
    <input style="text-transform: lowercase; id="login" name="login" maxlength="20" placeholder="Login ID" type="text" class="input-large "/><br><br>
    <label class="control-label" for="firstlastname">Name</label><br>
    <input name="firstlastname" maxlength="20" placeholder="Name" type="text" class="input-large"/><br><br>
    <label class="control-label" for="password">Password</label><br>
    <input name="password" maxlength="100" placeholder="Enter a Password ..." type="password" class="input-large"/> <br><br>
    <label class="control-label" for="email">Email</label><br>
    <input style="text-transform: lowercase; id="email" name="email" maxlength="20" placeholder="Email" type="text" class="input-large"/><br><br>   
    <div class="controls">
        <input type="submit" value="Submit!" name="submit">
    </div>
    </form>

And an 'index.erb' file which simply contains;

    <textarea style="width:460px; height:80px"><%= @u.inspect %> </textarea>

u.inspect will give us the currently logged in user information.

We also need to create a layout file which sinatra will look for by default, 'layout.erb';

    <!DOCTYPE html>
    <html>
    <head>
        <title>Flitter</title>
        <script src="/lib/jquery-1.8.3.min.js" type="text/javascript"></script> 
        <link href="/CSS/style.css" rel="stylesheet"> 
    </head>
    
    <body style="padding-top: 20px;">
    
    <% if $currentuser.nil? %>
    <div id="navbar"> 
        <ul> 
            <li><a href="/flitter">Flitter</a></li> 
            <li><a href="/flitter/user/login">Login</a></li> 
            <li><a href="/flitter/user/signup">Signup</a></li>
            <li><a href="/flitter/user/list">List Users</a></li>
        </ul> 
    </div> 
    <% else %>
    <div id="navbar"> 
        <ul> 
            <li><a href="/flitter">Flitter</a></li> 
            <li><a href= <%= '/flitter/' + $currentuser %>> <%= $currentuser %>'s Home</a></li> 
            <li><a href="/flitter/user/list">List Users</a></li>
            <li><a href="/flitter/user/logout">Logout</a></li>
        </ul> 
    </div> 
    <% end %>
    
    <div class="container">
    <br>
        <%if @error then %>
        <div class="alert alert-error"><%=@error%></div>
        <br>
        <%elsif @info then %>
        <div class="alert alert-info"><%=@error%></div>
        <br>
        <% end %>
        <%= yield %>
      </body>
    </html>

*note: A lot of this code is not strictly necessary at the moment but will be used in part II.

The ruby code here needs a little explanation. The block starting '<% if $currentuser.nil? %>' checks to see if a user is logged in or not and changes the navbar options accordingly.

The final block of code inside the '<%if @error then %>' block defines two types of message to send to the user. An error alert, and an info alert. Anyone familiar with twitter bootstrap should recognise these.

'<%= yield %>' calls ruby's yield function that will allow us to process the feedback messages from our application code.

We also need a CSS file; Make a new folder 'CSS' in the root directory of our app and create the file 'style.css';

    #navbar ul { 
            margin: 0; 
            padding: 5px; 
            list-style-type: none; 
            text-align: center; 
            background-color: #000; 
    }
    
    #navbar ul li {  
            display: inline; 
    }
    
    #navbar ul li a { 
            text-decoration: none; 
            padding: .2em 1em; 
            color: #fff; 
            background-color: #000; 
    }
    
    #navbar ul li a:hover { 
            color: #000; 
            background-color: #fff; 
    }
    
    .alert {
      margin-bottom: 5px;
      background-color: #fcf8e3;
      border: 1px solid #fbeed5; 
    }
    
    .alert-error {
      color: #b94a48;
      background-color: #f2dede;
      border-color: #eed3d7;
    }
    
    .alert-info {
      color: #3a87ad;
      background-color: #d9edf7;
      border-color: #bce8f1;
    }

And finally, add the code to allow logins to 'flitter'rb'.

    get '/flitter/user/login/?' do
        erb :login
    end
    
    post '/flitter/user/login/?' do
        name = params["login"]
        pass = params["password"]
    
        if session[:user] = User.authenticate(name, pass)
            $currentuser = name         
            redirect "/flitter"
        else
            @error = "Username and Password do not match"
            halt erb :login
        end
    end

Get 'login' and 'password' from the user. If the credentials are correct then assign the login name to $currentuser and redirect to the homepage. If credentials are incorrect then provide feedback to the user and halt the login erb.

And the 'login.erb' file.

    <form class ="form-signin" method="post" action="/flitter/user/login">
        <h3>Please login</h3><hr>
        <label class="control-label" for="id_username">Username</label><br>
        <input name="login" style="text-transform: lowercase; maxlength="100" placeholder="Enter your login" type="text" class="input-large"/><br><br>
        <label class="control-label" for="id_password">Password</label><br>
        <input name="password" maxlength="100" placeholder="Enter your password ..." type="password" class="input-large"/><br>
        <div class="controls"><br>
        <input type="submit" value="Submit!" name="submit">
        </div>
    </form>

And a 'list.erb' file to display a list of our users.

    <h4>Viewing All Users </h4>
    <hr>
            <% @u.each do |user| %>  
            <p>Username: <strong><%= user.login %></strong><br> 
            Email:  <strong><%= user.email %></strong><br> 
            Joined Flitter: <%= user.created_at %></p>
            <% end %>  
            <hr>
    <br>

And of course, add the code to handle logouts to 'flitter.rb'.

    get '/flitter/user/logout/?' do
        session[:user] = nil
        $currentuser = nil
        redirect '/flitter'
    end

Now we are ready to test our application. Go to the root folder and run;

	ruby flitter.rb

Go to your browser and browse to localhost:4567/flitter.

Where you should be greeted by a page which looks like the following;

Flitter Homepage Flitter Homepage

You can see a textbox with the word 'nil' in it. This textbox is holding the value of the current user.

Go ahead and signup a user with appropriate values and then login. You should now be directed back to the homepage and the value of the textbox will hold the value of your newly created user.

Flitter Homepage with User information Flitter Homepage

Of course there are numerous libraries that you could use to deal with user authentication such as Max Justus Spransy's sinatra-authentication library.

In part II we will add the ability to post content to the flitter app. 
