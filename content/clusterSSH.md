Title: Using ClusterSSH to administer multiple hosts  
Date: 2014-01-22 10:20
Category: Linux
Tags: Linux, devops
Slug: cluster_ssh
Summary: Using ClusterSSH to administer multiple hosts  

I recently had a small (but tedious) task to complete which involved logging into, and running identical commands on, multiple servers using ssh. I'd heard about tools to ssh to multiple machines before but I'd never used one. After a short google I came across ClusterSSH and as the description says;

    ClusterSSH controls a number of xterm windows via a single graphical console window to allow commands to be interactively run on multiple servers over an ssh connection.

Using it was as simple as adding the server names to a configuration file and launching the application.

You can add the host's names to '/etc/clusters' or '~.csshrc'. You can define clusters of machines like'

    webcluster username@hostname1 username@hostname2 username@hostname3 username@hostname4
    databasecluster username@hostname5 username@hostname6 username@hostname7 username@hostname8

And then simply;

    cssh webcluster

And it will open xterm windows ssh'd into the machines. It will open a small console window where you can input commands and these commands will be replicated in all the xterm windows.

Running nano on 4 hosts Running nano on 4 hosts
![Pelican](../images/clusterapt-get.png)

ClusterSSH running htop on 25 hosts ClusterSSH running htop on 25 hosts
![Pelican](../images/clusterhtop.png)
