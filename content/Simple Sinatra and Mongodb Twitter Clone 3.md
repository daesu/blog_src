Title: Simple twitter clone with Sinatra and Mongo; Part III
Date: 2013-03-22 10:20
Category: Tutorials
Tags: Ruby, Mongodb, Development
Slug: simple_sinatra_twitter_clone_3
Summary: Simple twitter clone with Sinatra and Mongo; Part III

Finally I'm going to add pagination and the ability to signup using facebook credentials to this twitter clone.

The full code will be available on github here. I'm only going to document the most relevant changes in this post.

So to implement pagination I used the great library will_paginate. To add this to the app we just need to require the library;

    require 'will_paginate'
    require 'will_paginate/array'
    require 'will_paginate/view_helpers'
    require 'will_paginate/view_helpers/link_renderer'

And to implement it in this app we must create a method to send the database information to the will_paginate functions.

    def pagination(limit, user)
    
        page = params[:page] || 1
        page = page.to_i
        page -= 1
        offset = limit * page
    
        if user.nil? 
            totalflitts = Flitt.all.count
            flitt = Flitt.all :limit => limit, :offset => offset, :order => :created_at.desc     
            @flitt = WillPaginate::Collection.create(page + 1, limit, totalflitts) {|p| p.replace flitt }
        else
            totalflitts = Flitt.where(:user_id => user ).all.count
            flitt = Flitt.where(:user_id => user ).all :limit => limit, :offset => offset, :order => :created_at.desc
            @flitt = WillPaginate::Collection.create(page + 1, limit, totalflitts) {|p| p.replace flitt }
        end
        return @flitt
    end

Here I have created a method named pagination which takes two values, limit, and user. Limit will be the value of how many entries per page to display and user will be the value of which user's flitts we wish to display.

The if loop has one simple condition. If we call pagination and the value for user is nil then we query and display all user's flitts. Else, we query and display only the user's flitts who was specified. @flitt will contain these values and will be returned to the calling method before been passed onto an erb file for display.

Speaking of which, all we need to add to an erb file is;

    <%= will_paginate @update %>

The default look for the pagination controls are rather wanting and forunately the will_paginate library can help here too.

I choose the Digg look, downloaded the necessary CSS file and added the necessary code to my erb file to achieve this look;

Flitter Digg style pagination Flitter Digg
![Pelican](../images/diggstyle.png)

Finally, to add Facebook connect support I did so following the guides available on the facebook developers site. The guides are quite easy to follow so I won't document the majority of the process here.

The most relevant part of this was making a call to facebook for information and using the response.

    function SubmitDetails() {
    
    var textname = document.getElementById('firstlastname');    
    var textemail = document.getElementById('email');
    var textlogin = document.getElementById('login');
    
    FB.api("/me?fields=name,email",
            function (response) {
    
                textname.value = response.name;
                textemail.value = response.email;
                var txtnamefromfb = response.name;
                textlogin.value = response.name.replace(/\s+/g, '');
    
            }, { perms: 'email' });    
    }

First, the three 'vars' declared are going to allow us to write to the text forms on our signup page to add the called facebook data to these fields.

The next part, 'FB.api' calls facebook asking for two values, a name, and an email address. No permissions are required to get the name but to get the email address we need to ask the user for permission which you can see in the code block "{ perms: 'email' " which will prompt the user for permission to access their email address information.

The response function itself simply gets the values returned from facebook which are contained in the response object and assigns the appropriate values to the signup form's textfields.

The final site is available, at least for a while, on heroku here. 



