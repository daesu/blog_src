Title: Sinatra and Mongodb Twitter Clone 
Date: 2013-01-01 10:20
Category: Tutorials
Tags: Sinatra, Mongodb, Ruby
Slug: sinatra_twitter_clone
Summary: Sinatra and Mongodb Twitter Clone 

This is the content of my super blog post.

I'm currently learning three different technologies at the moment. The Ruby programming language, the Sinatra web application framework, and the NoSQL database Mongo.

Many developers who end up using Mongo on Sinatra also choose to use an Object-Document-Mapper such as Mongo Mapper, Mongoid, and others to further ease usage of Mongo and provide a familiar API to developers familiar with other popular ODM's such as Data Mapper. I decided to use Mongo Mapper because it seems to be the most mature.

So first of all, the requirements.

You should have [MongoDB](http://www.mongodb.org/), Ruby, and [Sinatra](http://www.sinatrarb.com/) already installed.

You will need to install the mongo ruby driver, mongomapper, and bundler to manage the environment.

    gem install mongo
    gem install mongo_mapper
    gem install bundler

First of all create a folder for the test application.

    mkdir testApp

And next create the gemfile, adding our requirements and dependencies. This isn't strictly necessary but it's good practice.

    touch Gemfile
    echo "source :rubygems" >> Gemfile
    echo "gem 'sinatra'" >> Gemfile
    echo "gem 'mongo'" >> Gemfile
    echo "gem 'mongo_mapper'" >> Gemfile
    bundle install


So let's create a simple Sinatra app using Mongo. I'm going to make a very simple app to post updates like tweets on twitter. Create the app file;

    touch testApp.rb

And open it in your favourite text editor. I prefer nano

    nano testApp.rb

And add the following code;

    require 'bundler'
    require 'sinatra'
    require 'mongo_mapper'

Next we create a name for the database.

    MongoMapper.database = 'testApp'

Next create the class to store status updates.

    class Status
        include MongoMapper::Document
        key :content, type: String, :required => true
        timestamps!
    end

Here we define a class and tell it we want to create a Mongo document. There is only one field called content to store the status update and it is a required field. And lastly, timestamps is called to allow ordering by time.

Next we need to display the status updates on the main page.

    get '/' do
        @status = Status.all :order => :created_at.desc
        erb :home  
    end

The first line will be called when the get method is called at '/'. Next we query all the status updates in the database and order them by the created datetime. The next line calls an ERB file called home. ERB files are commonly used in Rugby to generate web pages.

Make a new directory called 'views' and create a new file named home.erb. This file simply set up a form to accept the status update content and displays status updates below that form.

    <form action="/" method="post">  
        <textarea name="content" ></textarea>  
        <input type="submit" value="Update!">  
    </form>  
    <% @status.each do |status| %>  
        <article <
            <p><%= status.content %></p>  
            <p class="meta"> Created: <%= status.created_at %></p>  
        </article>  
    <% end %>

And finally, back in the testApp.rb file we need to add the logic to deal with posts.

    post '/' do  
        update = Status.new  
        update.content = params[:content] 
        update.save  
        redirect '/'  
    end

When the post method is called at '/' create a new status update, add the content, save it and redirect to '/' to refresh and display.

To run, move to the testApp directory and run;

    ruby testApp.rb

Then navigate to localhost:4567 and test it out. 