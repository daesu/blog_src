Title: About

Software Engineer from Ireland living and working in Seoul, South Korea.

Interests: Open Source, Linux, Programming (Ruby, Python, Vala), Android.