Title: Linux Mint: Mate edition 
Date: 2016-01-05 10:20
Category: Linux
Tags: Linux, OS, Programming, Environment
Slug: linux_mint_mate
Summary: Linux Mint: Mate Edition  

After several years using [Gnome 3](https://www.gnome.org/gnome-3/) as well as a short period of time using OSX, I have decided to try something different. I did not feel I was as productive as I could be using Gnome Shell and OSX was even worse. 

I remembered the Gnome 2 days and decided to give the [MATE desktop environment](http://mate-desktop.org/) a try. 

For those who don't know, MATE is a desktop environment forked from the code base of GNOME 2. It is under active development and includes all of the GNOME 2 standard apps, with different names of course. 

The distribution I choose was [Linux Mint](http://mate.linuxmint.com/) as I had heard good things about it from colleagues and had never tried it before. 

Speaking a month into my MATE trial I cannot ever imagine going back to anything else. Gnome 2, and now MATE, are the most productive environments I have used. It might be because of my history using Gnome 2 for so long that I have this experience but I would recommend anyone to try it out. 

Linux Mint Mate Desktop
![Pelican](../images/mint_mate.png)