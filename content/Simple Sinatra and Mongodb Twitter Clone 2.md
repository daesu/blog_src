Title: Simple twitter clone with Sinatra and Mongo; Part II
Date: 2013-02-22 10:20
Category: Tutorials
Tags: Ruby, Mongodb, Development
Slug: simple_sinatra_twitter_clone_2
Summary: Simple twitter clone with Sinatra and Mongo; Part II
So after part I we now have a simple functioning login system which we will now expand upon to allow our users to post updates.

In the '/flitter/database/database.rb' file I have added the new class 'Flitt';

    class Flitt
    
        include MongoMapper::Document
    
        key :user_id
        key :content,   type: String,   :required => true
    
        timestamps!
    
    end

This sets up a MongoMapper document with two keys, a user_id, and content. We also have timestamps to document the created time of a flitt post.

In 'flitter.rb' I have changed 'get 'flitter/?' to include the line

    @update = Flitt.all :order => :created_at.desc

This will get all Flitt objects and return them ordered by time created.

I have added a new method to post a new Flitt;

    post "/flitter/:name" do  
        update = Flitt.new  
        update.user_id = $currentuser
        update.content = params[:content]
    
        name = $currentuser
    
        if update.save
            redirect "/flitter/#{name}"
        else
            @error = "Sorry something went wrong"
            halt erb :userhome
        end
    end

Here we create a new Flitt object and assign a value to it's user_id based on the currently logged in user. We get the content from the user and add it to the Flitt object's content field.

Next, there are two new methods;

    get "/flitter/:name" do 
        if $currentuser.nil?
            redirect "/flitter"
        elsif params[:name] != $currentuser
            redirect "/flitter/#{$currentuser}"
            erb :userhome               
        else
            @update = Flitt.where(:user_id => $currentuser ).all :order => :created_at.desc
            erb :userhome
        end
    end

'/flitter/username' is the user's private homepage on Flitter and will contain the forms to allow input of a new post. If a logged in user tries to go to another's homepage they are redirected to their own homepage.

The user's flitts are displayed underneath a posting form for new flitts.

    get "/flitter/user/:name/?" do
    
        exist = User.where(:login => params[:name] ).first
        if exist.nil?
            @error = "No such User exists"
            halt erb :userpublichome
        else
            @update = Flitt.where(:user_id => params[:name] ).all :order => :created_at.desc
            @who = params[:name]
            erb :userpublichome
        end
    end

'/flitter/user/username' is the user's public page on Flitter which is visible to everyone. All the flitts of that user are shown here.

It checks if the user exists and presents an error if the user wasn't found. If the user exists it shows all the flitts by that user.

There are two new erb files; userhome and userpublic home to accompany these new methods. Userhome has two important blocks;

    <div id="postingbox">
        <form action=<%= $currentuser %> method="post"> 
        <textarea id="txtcontent" name="content" maxlength="200" style="width:460px; height:80px"> </textarea>
        <br>
        <input type="text" id="counterBox" placeholder="200 Characters remaining" readonly/>
        <div class="controls">
        <input name="submitpost" type="submit" value=" Update! "> 
        </div>
        </form>
    <div>

This sets up the flitt posting box.

    <script>
        <!-- "Client side validation for num of characters" -->
        var txtcontentvalue = document.querySelector("#txtcontent");
        var countval = document.querySelector("#counterBox");
        txtcontentvalue.addEventListener("keydown",function(){
        var remaining = 0;
        remaining = 200 - parseInt(txtcontentvalue.value.length);
        if(remaining < 0)
        {
            txtcontentvalue.value = txtcontentvalue.value.substring(0, 200);
            return false;
        }
            countval.value = remaining + " Characters remaining";
        },true);
    
        <!-- "ignore enter key in content box" -->
        $(txtcontent).keypress(function(event) {
        if (event.keyCode == 13) {
        event.preventDefault();
        }
        });
    </script>

And here we have some javascript utilising jquery. The first javascript block limits the amount of characters for a flitt to 200. It also displays the remaining number of characters in a textbox underneath the posting box.

The second block of code makes the posting box ignore the Enter key to prevent users creating a multiline posting where each line would be counted as one character.

The rest of 'userhome.erb' is almost identical to 'userpublichome.erb' which contains the following;

    <% if @update.nil? %>
    <% else %>
    <h4>Viewing <%= @who %>'s Flitts </h4>
    <hr>
    <div id="listing" >
        <% @update.each do |update| %>  
        <% ago = (Time.now.utc.- update.created_at).round / 60 %>
        <article>
        <div id="content" style="padding: 5px;float:left;">
            <%= update.content %>
            </div>
                <br><br><br>
                <% if ago < 60 %>
                <p><small>Created <%= ago %> minutes ago</small></p>  
                <% elsif ago >= 60  %>
                <p><small>Created <%= ago / 60 %> hours ago</small></p>
                <% end %>
                <hr>
            </article>
            <br>
        <% end %>
    
    <% end %>
    </div>

Here, we get all the user's flitts and display them in formatted boxes described in our CSS file. We also display the lapsed time in minutes or hours since the flitt post was made.

The contents of 'index.erb' has also changed to be almost identical to this except it displays all users' flitts.

Run the application as before;

    ruby flitter.rb

Flitter Homepage displaying all users posts Flitter Homepage
![Pelican](../images/flitterhome.png)

Signup Errors Signup Errors
![Pelican](../images/signuperrors.png)

Userhome User Homepage
![Pelican](../images/userhome.png)

In the next part I will implement pagination to limit the number of flitt posts showing on each page and I will also implement facebook connect integration for user registration.