#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u"Pol O'Riain"
SITENAME = u'/.blog'
SITEURL = 'http://daesu.github.io'

PATH = 'content'

TIMEZONE = 'Asia/Seoul'

DEFAULT_LANG = u'en'

THEME = "pelican-themes/tuxlite_tbs"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),)

# Social widget
SOCIAL = (('Github', 'https://github.com/daesu/'),)

# SOCIAL = (('Github', 'https://github.com/daesu/'),
#           ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
